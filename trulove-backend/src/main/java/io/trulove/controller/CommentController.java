package io.trulove.controller;

import io.trulove.dto.CommentRequest;
import io.trulove.dto.CommentResponse;
import io.trulove.service.CommentService;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/comments")
@AllArgsConstructor
public class CommentController {

  private final CommentService commentService;

  @PostMapping
  public ResponseEntity<CommentResponse> addComment(@RequestBody CommentRequest commentRequest) {
    return new ResponseEntity<>(commentService.save(commentRequest), HttpStatus.CREATED);
  }

  @GetMapping("/post/{id}")
  public ResponseEntity<Page<CommentResponse>> getCommentsByPost(@PathVariable("id") Long id,
      @RequestParam Optional<Integer> page) {
    return new ResponseEntity<>(commentService.getCommentsForPost(id, page.orElse(0)),
        HttpStatus.OK);
  }

  @GetMapping("/user/{id}")
  public ResponseEntity<Page<CommentResponse>> getCommentsByUser(@PathVariable("id") Long id,
      @RequestParam Optional<Integer> page) {
    return new ResponseEntity<>(commentService.getCommentsForUser(id, page.orElse(0)),
        HttpStatus.OK);
  }
}
