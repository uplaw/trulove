package io.trulove.controller;

import io.trulove.dto.CommunityDTO;
import io.trulove.service.CommunityService;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/community")
@AllArgsConstructor
public class CommunityController {

  CommunityService communityService;

  @GetMapping("/{page}")
  public ResponseEntity<Page<CommunityDTO>> getAllCommunities(@PathVariable("page") Integer page) {
    return new ResponseEntity<>(communityService.getAll(page), HttpStatus.OK);
  }

  @GetMapping("/community/{id}")
  public ResponseEntity<CommunityDTO> getCommunity(@PathVariable("id") Long id) {
    return new ResponseEntity<>(communityService.getCommunity(id), HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<CommunityDTO> addCommunity(@RequestBody @Valid CommunityDTO communityDTO)
      throws Exception {
    try {
      return new ResponseEntity<>(communityService.save(communityDTO), HttpStatus.OK);
    } catch (Exception e) {
      throw new Exception("Error Creating Community");
    }
  }
}
