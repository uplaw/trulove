package io.trulove.controller;

import io.trulove.dto.AuthResponse;
import io.trulove.dto.LoginRequest;
import io.trulove.dto.RefreshTokenRequest;
import io.trulove.dto.RegisterRequest;
import io.trulove.service.AuthService;
import io.trulove.service.RefreshTokenService;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@AllArgsConstructor
public class AuthController {

  AuthService authService;
  RefreshTokenService refreshTokenService;

  @PostMapping("/register")
  public ResponseEntity register(@RequestBody RegisterRequest registerRequest) {
    authService.register(registerRequest);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @GetMapping("/verify/{token}")
  public ResponseEntity verify(@PathVariable String token) {
    authService.verifyToken(token);
    return new ResponseEntity<>("Account Activated", HttpStatus.OK);
  }

  @PostMapping("/login")
  public AuthResponse register(@RequestBody LoginRequest loginRequest) {
    return authService.login(loginRequest);
  }

  @PostMapping("/refresh/token")
  public AuthResponse refreshToken(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
    return authService.refreshToken(refreshTokenRequest);
  }

  @PostMapping("/logout")
  public ResponseEntity<String> logout(
      @Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
    refreshTokenService.deleteRefreshToken(refreshTokenRequest.getRefreshToken());
    return ResponseEntity.status(HttpStatus.OK).body("Refresh Token Deleted");
  }
}
