package io.trulove.model;

import java.time.Instant;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Post {

  @Id
  @SequenceGenerator(name = "POST_GEN", sequenceName = "SEQ_POST", allocationSize = 1)

  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "POST_GEN")
  private Long postId;

  @NotBlank(message = "Post Title is required")
  private String postTitle;

  @Nullable
  private String url;

  @Nullable
  @Lob
  private String description;

  private Integer voteCount;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "userId", referencedColumnName = "userId")
  private User user;

  private Instant dateCreated;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "id", referencedColumnName = "id")
  private Community community;
}
