package io.trulove.exception;

public class TruLoveException extends RuntimeException {

  public TruLoveException(String message) {
    super(message);
  }
}
