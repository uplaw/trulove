package io.trulove.exception;

public class VoteException extends RuntimeException {

  public VoteException(String message) {
    super(message);
  }
}
