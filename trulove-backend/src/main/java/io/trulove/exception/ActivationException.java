package io.trulove.exception;

public class ActivationException extends RuntimeException {

  public ActivationException(String message) {
    super(message);
  }
}
