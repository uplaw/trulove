package io.trulove;

import io.trulove.model.Community;
import io.trulove.model.Post;
import io.trulove.model.User;
import io.trulove.repository.CommunityRepository;
import io.trulove.repository.PostRepository;
import io.trulove.repository.UserRepository;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User.UserBuilder;

@Configuration
public class ApplicationConfig {

  @Autowired
  UserRepository userRepository;

  @Autowired
  PostRepository postRepository;

  @Autowired
  CommunityRepository communityRepository;


  @Bean
  CommandLineRunner runner() {
    return args -> {
      initTrulove();

    };
  }



  private void initTrulove() {
    User user1 = new User();
    user1.setEmail("user1@email.com");
    user1.setPassword("password");
    user1.setUsername("user");
    userRepository.save(user1);

    Community community1 = new Community();
    community1.setDescription("Community 1 descritption for com 1 people.");
    community1.setName("Community 1");
    community1.setUser(user1);
    community1.setDateCreated(Instant.now());
    community1.setDescription("Community 1 descritption for com 1 people.");
    communityRepository.save(community1);


    Post post1 = new Post();
    post1.setPostTitle("Post 1 Title");
    post1.setUser(user1);
    post1.setVoteCount(2);
    post1.setCommunity(community1);
    post1.setDateCreated(Instant.now());
    post1.setDescription("Post 1 Description");
    post1.setUrl("post/post-1-title");
    postRepository.save(post1);

    List<Post> posts = new ArrayList<>();



  }

}
