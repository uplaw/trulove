package io.trulove.service;

import io.trulove.exception.TruLoveException;
import io.trulove.model.RefreshToken;
import io.trulove.repository.RefreshTokenRepository;
import java.time.Instant;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Transactional
public class RefreshTokenService {

  private RefreshTokenRepository refreshTokenRepository;

  RefreshToken generateRefreshToken() {
    RefreshToken refreshToken = new RefreshToken();
    refreshToken.setToken(UUID.randomUUID().toString());
    refreshToken.setCreationDate(Instant.now());
    return refreshTokenRepository.save(refreshToken);
  }

  void validateToken(String token) {
    refreshTokenRepository.findByToken(token)
        .orElseThrow(() -> new TruLoveException("Invalid Refresh Token"));
  }

  public void deleteRefreshToken(String token) {
    refreshTokenRepository.deleteByToken(token);
  }
}
