package io.trulove.service;

import com.github.marlonlom.utilities.timeago.TimeAgo;
import io.trulove.dto.PostRequest;
import io.trulove.dto.PostResponse;
import io.trulove.exception.CommunityNotFoundException;
import io.trulove.exception.PostNotFoundException;
import io.trulove.exception.UserNotFoundException;
import io.trulove.model.Community;
import io.trulove.model.Post;
import io.trulove.model.User;
import io.trulove.model.Vote;
import io.trulove.model.VoteType;
import io.trulove.repository.CommentRepository;
import io.trulove.repository.CommunityRepository;
import io.trulove.repository.PostRepository;
import io.trulove.repository.UserRepository;
import io.trulove.repository.VoteRepository;
import java.time.Instant;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Transactional
public class PostService {

  private final PostRepository postRepository;
  private final CommunityRepository communityRepository;
  private final UserRepository userRepository;
  private final CommentRepository commentRepository;
  private final AuthService authService;
  private final VoteRepository voteRepository;

  private boolean checkVoteType(Post post, VoteType voteType) {
    if (authService.isLoggedIn()) {
      Optional<Vote> voteForPostForUser = voteRepository
          .findTopByPostAndUserOrderByVoteIdDesc(post, authService.getCurrentUser());
      return voteForPostForUser.filter(vote -> vote.getVoteType().equals(voteType)).isPresent();
    }
    return false;
  }

  private PostResponse mapToResponse(Post post) {
    return PostResponse.builder()
        .postId(post.getPostId())
        .postTitle(post.getPostTitle())
        .url(post.getUrl())
        .description(post.getDescription())
        .userName(post.getUser().getUsername())
        .communityName(post.getCommunity().getName())
        .voteCount(post.getVoteCount())
        .commentCount(commentRepository.findAllByPost(post).size())
        .duration(TimeAgo.using(post.getDateCreated().toEpochMilli()))
        .upVote(checkVoteType(post, VoteType.UPVOTE))
        .downVote(checkVoteType(post, VoteType.DOWNVOTE))
        .build();
  }

  private Post mapToPost(PostRequest postRequest) {
    Community community = communityRepository.findByName(postRequest.getCommunityName())
        .orElseThrow(() -> new CommunityNotFoundException(postRequest.getCommunityName()));
    Post newPost = Post.builder()
        .postTitle(postRequest.getPostTitle())
        .url(postRequest.getUrl())
        .description(postRequest.getDescription())
        .voteCount(0)
        .user(authService.getCurrentUser())
        .dateCreated(Instant.now())
        .community(community)
        .build();
    community.getPosts().add(newPost);
    return newPost;
  }

  public PostResponse save(PostRequest postRequest) {
    return mapToResponse(postRepository.save(mapToPost(postRequest)));
  }

  public Page<PostResponse> getAllPost(Integer page) {
    return postRepository.findAll(PageRequest.of(page, 100)).map(this::mapToResponse);
  }

  public PostResponse findByID(Long id) {
    Post post = postRepository.findById(id)
        .orElseThrow(() -> new PostNotFoundException("Post not found with id: " + id));
    return mapToResponse(post);
  }

  public Page<PostResponse> getPostsByCommunity(Integer page, Long id) {
    Community community = communityRepository.findById(id)
        .orElseThrow(() -> new CommunityNotFoundException("Community not found with id: " + id));
    return postRepository
        .findAllByCommunity(community, PageRequest.of(page, 100))
        .map(this::mapToResponse);
  }

  public Page<PostResponse> getPostsByUsername(String username, Integer page) {
    User user = userRepository.findByUsername(username)
        .orElseThrow(() -> new UserNotFoundException("User not found with username: " + username));
    return postRepository
        .findByUser(user, PageRequest.of(page, 100))
        .map(this::mapToResponse);
  }
}
