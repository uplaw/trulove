package io.trulove.service;

import com.github.marlonlom.utilities.timeago.TimeAgo;
import io.trulove.dto.CommentRequest;
import io.trulove.dto.CommentResponse;
import io.trulove.exception.PostNotFoundException;
import io.trulove.exception.UserNotFoundException;
import io.trulove.model.Comment;
import io.trulove.model.Post;
import io.trulove.model.User;
import io.trulove.repository.CommentRepository;
import io.trulove.repository.PostRepository;
import io.trulove.repository.UserRepository;
import java.time.Instant;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Transactional
public class CommentService {

  private final UserRepository userRepository;
  private final PostRepository postRepository;
  private final CommentRepository commentRepository;
  private final AuthService authService;

  private CommentResponse mapToResponse(Comment comment) {
    return CommentResponse.builder()
        .id(comment.getId())
        .text(comment.getText())
        .postId(comment.getPost().getPostId())
        .dateCreated(TimeAgo.using(comment.getCreationDate().toEpochMilli()))
        .userName(comment.getUser().getUsername())
        .build();
  }

  private Comment mapToComment(CommentRequest commentRequest) {
    User user = authService.getCurrentUser();
    Post post = postRepository.findById(commentRequest.getPostId())
        .orElseThrow(() -> new PostNotFoundException(
            "Post not found with id: " + commentRequest.getPostId()));
    return Comment.builder()
        .text(commentRequest.getText())
        .post(post)
        .creationDate(Instant.now())
        .user(user)
        .build();
  }

  public CommentResponse save(CommentRequest commentRequest) {
    return mapToResponse(commentRepository.save(mapToComment(commentRequest)));
  }

  public Page<CommentResponse> getCommentsForPost(Long id, Integer page) {
    Post post = postRepository.findById(id)
        .orElseThrow(() -> new PostNotFoundException("Post not found with id: " + id));
    return commentRepository.findByPost(post, PageRequest.of(page, 100)).map(this::mapToResponse);
  }

  public Page<CommentResponse> getCommentsForUser(Long id, Integer page) {
    User user = userRepository.findById(id)
        .orElseThrow(() -> new UserNotFoundException("User not found with id: " + id));
    return commentRepository.findAllByUser(user, PageRequest.of(page, 100))
        .map(this::mapToResponse);
  }
}
