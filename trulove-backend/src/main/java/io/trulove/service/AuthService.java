package io.trulove.service;

import io.trulove.config.Constants;
import io.trulove.dto.AuthResponse;
import io.trulove.dto.LoginRequest;
import io.trulove.dto.RefreshTokenRequest;
import io.trulove.dto.RegisterRequest;
import io.trulove.exception.ActivationException;
import io.trulove.model.AccountVerificationToken;
import io.trulove.model.NotificationEmail;
import io.trulove.model.User;
import io.trulove.repository.TokenRepository;
import io.trulove.repository.UserRepository;
import io.trulove.security.JWTProvider;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor

public class AuthService {

  UserRepository userRepository;
  PasswordEncoder passwordEncoder;
  TokenRepository tokenRepository;
  MailService mailService;
  MailBuilder mailBuilder;
  AuthenticationManager authenticationManager;
  JWTProvider jwtProvider;
  RefreshTokenService refreshTokenService;

  @Transactional
  public void register(RegisterRequest registerRequest) {
    User user = new User();
    user.setUsername(registerRequest.getUsername());
    user.setEmail(registerRequest.getEmail());
    user.setPassword(encodePassword(registerRequest.getPassword()));
    user.setCreationDate(Instant.now());
    user.setAccountStatus(false);

    userRepository.save(user);

    String token = generateToken(user);
    String message = mailBuilder.build("Welcome to React-Spring-Reddit Clone. " +
        "Please visit the link below to activate you account : " + Constants.EMAIL_ACTIVATION + "/"
        + token);
    mailService
        .sendEmail(new NotificationEmail("Please Activate Your Account", user.getEmail(), message));
  }

  @Transactional(readOnly = true)
  public User getCurrentUser() {
    org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder
        .
            getContext().getAuthentication().getPrincipal();
    return userRepository.findByUsername(principal.getUsername())
        .orElseThrow(() -> new UsernameNotFoundException(
            "User not found with username: " + principal.getUsername()));
  }

  public AuthResponse refreshToken(RefreshTokenRequest refreshTokenRequest) {
    refreshTokenService.validateToken(refreshTokenRequest.getRefreshToken());
    String token = jwtProvider.generateTokenWithUsername(refreshTokenRequest.getUsername());
    return new AuthResponse(token, refreshTokenService.generateRefreshToken().getToken(),
        Instant.now().plusMillis(jwtProvider.getJwtExpirationMillis()),
        refreshTokenRequest.getUsername());
  }

  public AuthResponse login(LoginRequest loginRequest) {
    Authentication authenticate = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
            loginRequest.getUsername(), loginRequest.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(authenticate);
    String authToken = jwtProvider.generateToken(authenticate);
    String refreshToken = refreshTokenService.generateRefreshToken().getToken();
    return new AuthResponse(authToken, refreshToken,
        Instant.now().plusMillis(jwtProvider.getJwtExpirationMillis()), loginRequest.getUsername());
  }

  private String encodePassword(String password) {
    return passwordEncoder.encode(password);
  }

  private String generateToken(User user) {
    String token = UUID.randomUUID().toString();
    AccountVerificationToken verificationToken = new AccountVerificationToken();
    verificationToken.setToken(token);
    verificationToken.setUser(user);
    tokenRepository.save(verificationToken);
    return token;
  }

  public void verifyToken(String token) {
    Optional<AccountVerificationToken> verificationToken = tokenRepository.findByToken(token);
    verificationToken.orElseThrow(() -> new ActivationException("Invalid Activation Token"));
    enableAccount(verificationToken.get());
  }

  public void enableAccount(AccountVerificationToken token) {
    String username = token.getUser().getUsername();
    User user = userRepository.findByUsername(username)
        .orElseThrow(() -> new ActivationException("User not found with username: " + username));
    user.setAccountStatus(true);
    userRepository.save(user);
  }

  public boolean isLoggedIn() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return !(authentication instanceof AnonymousAuthenticationToken) && authentication
        .isAuthenticated();
  }
}
