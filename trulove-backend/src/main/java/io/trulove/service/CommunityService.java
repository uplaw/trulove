package io.trulove.service;

import io.trulove.dto.CommunityDTO;
import io.trulove.exception.CommunityNotFoundException;
import io.trulove.model.Community;
import io.trulove.repository.CommunityRepository;
import java.time.Instant;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class CommunityService {

  private final CommunityRepository communityRepository;
  private final AuthService authService;

  private CommunityDTO mapToDTO(Community community) {
    return CommunityDTO.builder()
        .id(community.getId())
        .name(community.getName())
        .description(community.getDescription())
        .postCount(community.getPosts().size())
        .build();

  }

  private Community mapToCommunity(CommunityDTO communityDTO) {
    return Community.builder().name("/r/" + communityDTO.getName())
        .description(communityDTO.getDescription())
        .user(authService.getCurrentUser())
        .dateCreated(Instant.now())
        .build();

  }

  @Transactional(readOnly = true)
  public Page<CommunityDTO> getAll(Integer page) {
    return communityRepository.findAll(PageRequest.of(page, 100))
        .map(this::mapToDTO);
  }

  @Transactional
  public CommunityDTO save(CommunityDTO communityDTO) throws Exception {
    try {
      Community community = communityRepository.save(mapToCommunity(communityDTO));
      communityDTO.setId(community.getId());
      return communityDTO;
    } catch (Exception e) {
      throw new Exception("HEREAAAAAAA");
    }
  }

  @Transactional(readOnly = true)
  public CommunityDTO getCommunity(Long id) {
    Community community = communityRepository.findById(id)
        .orElseThrow(() -> new CommunityNotFoundException("Community not found with id -" + id));
    return mapToDTO(community);
  }
}
