package io.trulove.dto;

import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthResponse {

  private String authenticationToken;
  private String refreshToken;
  private Instant expiresAt;
  private String username;
}
