package io.trulove.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommunityDTO {

  private Long id;
  private String name;
  private String description;
  private Integer postCount;
}
