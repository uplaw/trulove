package io.trulove.repository;

import io.trulove.model.AccountVerificationToken;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface TokenRepository extends CrudRepository<AccountVerificationToken, Long> {

  Optional<AccountVerificationToken> findByToken(String token);
}
