package io.trulove.repository;

import io.trulove.model.Community;
import io.trulove.model.Post;
import io.trulove.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PostRepository extends PagingAndSortingRepository<Post, Long> {

  Page<Post> findAllByCommunity(Community community, Pageable pageable);

  Page<Post> findByUser(User user, Pageable pageable);
}
