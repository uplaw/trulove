package io.trulove.repository;

import io.trulove.model.Community;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CommunityRepository extends PagingAndSortingRepository<Community, Long> {

  Optional<Community> findByName(String communityName);

  Optional<Page<Community>> findByNameLike(String communityName, Pageable pageable);
}
