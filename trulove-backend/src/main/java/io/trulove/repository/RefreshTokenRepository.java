package io.trulove.repository;

import io.trulove.model.RefreshToken;
import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RefreshTokenRepository extends PagingAndSortingRepository<RefreshToken, Long> {

  Optional<RefreshToken> findByToken(String token);

  void deleteByToken(String token);
}
