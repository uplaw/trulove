package io.trulove.repository;

import io.trulove.model.Comment;
import io.trulove.model.Post;
import io.trulove.model.User;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CommentRepository extends PagingAndSortingRepository<Comment, Long> {

  Page<Comment> findByPost(Post post, Pageable pageable);

  List<Comment> findAllByPost(Post post);

  Page<Comment> findAllByUser(User user, Pageable pageable);
}
