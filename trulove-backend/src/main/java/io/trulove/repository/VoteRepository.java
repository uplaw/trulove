package io.trulove.repository;

import io.trulove.model.Post;
import io.trulove.model.User;
import io.trulove.model.Vote;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface VoteRepository extends CrudRepository<Vote, Long> {

  Optional<Vote> findTopByPostAndUserOrderByVoteIdDesc(Post post, User currentUser);
}
